package us.theappacademy.redditreader;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RedditPostTask extends AsyncTask<RedditListFragment, Void, JSONObject> {

    private RedditListFragment redditListFragment;

    @Override
    protected JSONObject doInBackground(RedditListFragment... redditListFragments) {
        JSONObject jsonObject = null;

        redditListFragment = redditListFragments[0];

        try {
            URL redditFeedurl = new URL("https://reddit.com/r/JUSTINBIEBER.json");

            HttpURLConnection httpConection = (HttpURLConnection)redditFeedurl.openConnection();
            httpConection.connect();

            int statusCode = httpConection.getResponseCode();

            if(statusCode == HttpURLConnection.HTTP_OK) {
                jsonObject = RedditPostParser.getInstance().parseInputStream(httpConection.getInputStream());

            }
        }
        catch (MalformedURLException error){
            Log.e("RedditPostTask", "MalformedURLException (doInbackground): " + error);
        }
        catch(IOException error){
            Log.e("RedditPostTask", "MalformedURLException (doInbackground): " + error);
        }

        return jsonObject;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        RedditPostParser.getInstance().readRedditFeed(jsonObject);
        redditListFragment.updateUserInterFace();
    }
}
